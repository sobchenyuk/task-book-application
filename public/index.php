<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);


require __DIR__ . '/../config/autoload.php';

// Instantiate settings
require __DIR__ . '/../app/settings.php';


$dotenv = Dotenv\Dotenv::create(ROOT_PATH);
$dotenv->load();

// Register routes
require __DIR__ . '/../app/routes.php';