# Task book application

task book application

install
1) npm install
2) composer install

run frontend watcher
1) npm run watch

insert:

__backend

composer,
"twig/twig": "^2.0",
"bramus/router": "~1.3",
"vlucas/phpdotenv": "^3.3"
"symfony/var-dumper": "^4.2"

__frontend

laravel-mix,
vue,
vuex,
style bootstrap-4,
