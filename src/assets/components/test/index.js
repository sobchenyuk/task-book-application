import Vue from 'vue';

import TestComponent from "./TestComponent.vue";

new Vue({
    el         : '#test-component',
    components : { TestComponent },
    template   : '<TestComponent/>'
});