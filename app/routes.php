<?php

$router = new \Bramus\Router\Router();

$router->setNamespace('App\Controller');
$router->get('/', 'PageController@home');

$router->set404(function() {
    header('HTTP/1.1 404 Not Found');
    include ROOT_PATH . '/404.html';
});

// Before Router Middleware
$router->before('GET', '/.*', function () {
    header('X-Powered-By: bramus/router');
});

$router->run();