<?php


define('ROOT_PATH', str_replace("/", DIRECTORY_SEPARATOR, realpath(__DIR__ . '/../') . '/'));

define('PUBLIC_PATH', ROOT_PATH . 'public' . DIRECTORY_SEPARATOR);

define('RESOURCE_PATH', ROOT_PATH . 'resource' . DIRECTORY_SEPARATOR);

define( 'TEMPLATE_PATH', ROOT_PATH . 'src' . DIRECTORY_SEPARATOR);
