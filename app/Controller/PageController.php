<?php

namespace App\Controller;


class PageController extends BaseController
{

    public function __invoke()
    {
        // TODO: Implement __invoke() method.
    }

    public function home() {
        echo $this->render('index.twig' );
    }

    public function openSource() {
        echo $this->render('open_source.twig' );
    }

    public function portfolio() {
        echo $this->render('portfolio.twig' );
    }

    public function contact() {
        echo $this->render('contact.twig' );
    }
}