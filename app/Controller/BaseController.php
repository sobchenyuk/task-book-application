<?php

namespace App\Controller;

// Twig_Loader_Filesystem
use Twig_Extension_Debug;
use Twig_Loader_Filesystem;
use Twig_Environment;
use Twig\TwigFunction;

class BaseController
{
    private $twig;

    public function __construct()
    {
        $loader = new Twig_Loader_Filesystem(TEMPLATE_PATH . '/views');
        $this->twig = new Twig_Environment($loader, array('debug' => true));
        $this->twig->addExtension(new Twig_Extension_Debug());
        $this->twig->addFunction($this->getFunctions());
    }

    public function render($templateName, array $parameters = array())
    {
        return $this->twig->render($templateName, $parameters);
    }


    public function getFunctions()
    {
        return new TwigFunction('getenv', [$this, 'getenv']);
    }

    public function getenv($varname)
    {
        $value = $_ENV[$varname];

        return $value;
    }
}